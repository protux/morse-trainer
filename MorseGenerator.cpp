#include "MorseGenerator.h"

int MorseGenerator::getCurrentSymbolLength() {
  // return sizeof(currentSymbol) / sizeof(char);
  return currentSymbol.length();
}

void MorseGenerator::updateTimeSinceLastUpdate() {
  long now = millis();
  timeSinceLastUpdate += (now - timeOfLastUpdate);
  timeOfLastUpdate = now;
}

void MorseGenerator::loadNewMorseSymbols() {
  char symbol = message.charAt(0);
  if (symbol == ' ') {
    currentSymbol = WORD_SEPARATOR;
  } else {
    currentSymbol = morseAlphabet->getMorseForSymbol(symbol);
    stretchCurrentSymbol();
  }
}

void MorseGenerator::increaseMessageIndex() {
  currentMessageIndex++;

  if (currentMessageIndex >= getCurrentSymbolLength()) {
    currentMessageIndex = 0;
    message = message.substring(1);
    if (message.length() == 0) {
      deactivate();
    } else {
      loadNewMorseSymbols();
    }
  }
}

bool MorseGenerator::isSwitchSymbols() {
  return getDurationOfCurrentSymbol() <= timeSinceLastUpdate;
}

void MorseGenerator::switchSymbols() {
  increaseMessageIndex();
  timeSinceLastUpdate = 0;
  int voltage = getVoltageStateOfCurrentSymbol();
  if (voltage == HIGH && isActive()) {
    tone(morsePin, 700);
  } else {
    noTone(morsePin);
  }
}

int MorseGenerator::getDurationOfCurrentSymbol() {
  if (currentMessageIndex < 0 || currentMessageIndex >= getCurrentSymbolLength()) {
    return 0;
  }

  switch (currentSymbol[currentMessageIndex]) {
    case DAH:
      return DAH_DURATION;
    case DIT:
      return DIT_DURATION;
    case MORSE_SYMBOL_SEPARATOR:
      return MORSE_SYMBOL_SPACE_DURATION;
    case WORD_SEPARATOR:
      return WORD_SPACE_DURATION;
    case SYMBOL_SEPARATOR:
      return SYMBOL_SPACE_DURATION;
    default:
      return 0;
  }
}

int MorseGenerator::getVoltageStateOfCurrentSymbol() {
  if (currentMessageIndex < 0 || currentMessageIndex >= getCurrentSymbolLength()) {
    return LOW;
  }

  switch (currentSymbol[currentMessageIndex]) {
    case DAH:
    case DIT:
      return HIGH;

    case ' ':
    default:
      return LOW;
  }
}

void MorseGenerator::stretchCurrentSymbol() {
  for (int i = currentSymbol.length() - 1; i > 0; i--) {
    currentSymbol = currentSymbol.substring(0, i) + " " + currentSymbol.substring(i);
  }
  if (message.length() > 1 && message.charAt(1) != ' ') {
    currentSymbol += SYMBOL_SEPARATOR;
  }
}

MorseGenerator::MorseGenerator(MorseAlphabet* morseAlphabet, byte morsePin) {
  this->morseAlphabet = morseAlphabet;
  this->morsePin = morsePin;
}

void MorseGenerator::setup() {
  pinMode(morsePin, OUTPUT);
}

void MorseGenerator::activate() {
  active = true;
  currentMessageIndex = -1;
  timeSinceLastUpdate = 0;
}

void MorseGenerator::deactivate() {
  active = false;
  noTone(morsePin);
}

bool MorseGenerator::isActive() {
  return active;
}

void MorseGenerator::update() {
  updateTimeSinceLastUpdate();

  if (isActive() && isSwitchSymbols()) {
    switchSymbols();
  }
}

void MorseGenerator::reinitialize(String message) {
  currentMessageIndex = -1;
  this->message = message;
  this->message.toUpperCase();
  loadNewMorseSymbols();
}
