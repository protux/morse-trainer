#include "T9.h"

void T9::pressKey(char key) {
  long now = millis();

  if (activeKey == EMPTY_CHAR) {
    initializeKey(key);
    printScreen(text + String(getCurrentCandidate()));
  } else if (key != activeKey || isTimeout(now)) {
    acceptSelection(key);
  } else {
    currentSelectionCounter += 1;
    printScreen(text + String(getCurrentCandidate()));
  }

  lastActionTimestamp = now;
}

void T9::initializeKey(char key) {
  currentSelectionCounter = 0;
  activeKey = key;
}

bool T9::isTimeout(long now) {
  return now - lastActionTimestamp >= CHAR_SELECTION_TIME_THRESHOLD;
}

void T9::acceptSelection(char key) {
  char currentCandidate = getCurrentCandidate();
  if (currentCandidate != EMPTY_CHAR) {
    String currentCandidateString = String(getCurrentCandidate());
    text += currentCandidateString;
    printScreen(text);
  }
  initializeKey(key);
}

void T9::printScreen(String textToPrint) {
  screen->display(prefix + textToPrint);
}

T9::T9(Screen* screen) {
  this->screen = screen;
}

String T9::getText() {
  return text;
}

char T9::getCurrentCandidate() {
  switch (activeKey) {
    case '0':
      return ((char[]) {
        ' ', '0'
      })[currentSelectionCounter % 2];
    case '1':
      return ((char[]) {
        '.', ',', '?', '=', '1'
      })[currentSelectionCounter % 5];
    case '2':
      return ((char[]) {
        'A', 'B', 'C', '2'
      })[currentSelectionCounter % 4];
    case '3':
      return ((char[]) {
        'D', 'E', 'F', '3'
      })[currentSelectionCounter % 4];
    case '4':
      return ((char[]) {
        'G', 'H', 'I', '4'
      })[currentSelectionCounter % 4];
    case '5':
      return ((char[]) {
        'J', 'K', 'L', '5'
      })[currentSelectionCounter % 4];
    case '6':
      return ((char[]) {
        'M', 'N', 'O', '6'
      })[currentSelectionCounter % 4];
    case '7':
      return ((char[]) {
        'P', 'Q', 'R', 'S', '7'
      })[currentSelectionCounter % 5];
    case '8':
      return ((char[]) {
        'T', 'U', 'V', '8'
      })[currentSelectionCounter % 4];
    case '9':
      return ((char[]) {
        'W', 'X', 'Y', 'Z', '9'
      })[currentSelectionCounter % 5];
    default:
      return EMPTY_CHAR;
  }
}

void T9::setup() {
  customKeypad.begin();
}

void T9::update() {
  customKeypad.tick();
  if (isTimeout(millis())) {
    acceptSelection(EMPTY_CHAR);
  }

  while (customKeypad.available()) {
    keypadEvent e = customKeypad.read();
    if (e.bit.EVENT == KEY_JUST_RELEASED) {
      char pressedKey = (char)e.bit.KEY;
      if (pressedKey != '#' && pressedKey != '*') {
        pressKey(pressedKey);
      }
    }
  }
}

void T9::clear() {
  text = "";
  initializeKey(EMPTY_CHAR);
  screen->display(text);
}

void T9::setPrefix(String prefix) {
  this->prefix = prefix;
}
