#ifndef _T9_h
#define _T9_h

#define EMPTY_CHAR '-'
#define CHAR_SELECTION_TIME_THRESHOLD 1000


#define ROWS 4
#define COLS 3

#include "string.h"
#include "Adafruit_Keypad.h"
#include "Arduino.h"

#include "Screen.h"

class T9 {
    const byte rowPins[ROWS] = {12, 11, 10, 9};
    const byte colPins[COLS] = {8, 7, 6};
    const char keys[ROWS][COLS] = {
      {'1', '2', '3'}, //,'A'},
      {'4', '5', '6'}, //,'B'},
      {'7', '8', '9'}, //,'C'},
      {'*','0','#'}, //,'D'}
    };
    Adafruit_Keypad customKeypad = Adafruit_Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);

    Screen* screen;
    byte currentSelectionCounter;
    long lastActionTimestamp;
    char activeKey;
    String text;
    String prefix;

    void pressKey(char key);
    void initializeKey(char key);
    bool isTimeout(long now);
    void acceptSelection(char key);
    void printScreen(String textToPrint);

  public:
    T9(Screen* screen);
    String getText();
    char getCurrentCandidate();
    void setup();
    void update();
    void clear();
    void setPrefix(String prefix);
};

#endif
