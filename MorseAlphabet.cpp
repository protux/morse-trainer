#include "MorseAlphabet.h"

int MorseAlphabet::findIndexOfSymbolInArray(char symbol) {
  for (int i = 0; i < SYMBOL_COUNT; i++) {
    if (symbol == morseAlphabet[i][0]) {
      return i;
    }
  }
  return -1;
}

char* MorseAlphabet::getMorseForSymbol(char symbol) {
  int index = findIndexOfSymbolInArray(symbol);
  if (index >= 0) {
    return morseAlphabet[index][1];
  }
  return "";
}

char MorseAlphabet::getRandomSymbol() {
  int index = random(SYMBOL_COUNT);
  return morseAlphabet[index][0];
}
