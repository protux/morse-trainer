#include "MorseToTextGame.h"

void MorseToTextGame::generateNewText() {
  currentText = String(morseAlphabet->getRandomSymbol());
}

void MorseToTextGame::wrongAnswer() {
  t9->clear();
  morseGenerator->activate();
}

void MorseToTextGame::correctAnswer() {
  t9->clear();
  start();
}

MorseToTextGame::MorseToTextGame(MorseAlphabet* morseAlphabet, MorseGenerator* morseGenerator, T9* t9) {
  this->morseAlphabet = morseAlphabet;
  this->morseGenerator = morseGenerator;
  this->t9 = t9;
}

void MorseToTextGame::start() {
  active = true;
  t9->clear();
  t9->setPrefix("Your Answer: ");
  generateNewText();
  morseGenerator->reinitialize(currentText);
  morseGenerator->activate();
}

void MorseToTextGame::stop() {
  active = false;
}

bool MorseToTextGame::isActive() {
  return active;
}

void MorseToTextGame::answer(String answer) {
  if (answer == currentText) {
    correctAnswer();
  } else {
    wrongAnswer();
  }
}

void MorseToTextGame::update() {
  if (t9->getText().length() > 0) {
    answer(t9->getText());
  }
}
