#ifndef _Screen_h
#define _Screen_h

#define RS_PIN 12
#define ENABLE_PIN 11
#define D4 3
#define D5 2
#define D6 1
#define D7 0

#define SCREEN_COLUMNS 16
#define SCREEN_ROWS 2
#define SCREEN_SIZE SCREEN_COLUMNS * SCREEN_ROWS

// #include <LiquidCrystal.h>
#include <LiquidCrystal_I2C.h>
#include "string.h"

class Screen {
    // LiquidCrystal lcd;
    LiquidCrystal_I2C  lcd; // 0x27 is the I2C bus address for an unmodified backpack


    void printRows(String text);
    void printRow(int rowIndex, String chunk);
    String cropTextBuffer(String text);

  public:
    // Screen() : lcd(RS_PIN, ENABLE_PIN, D4, D5, D6, D7) {}
    Screen() : lcd(0x27, 2, 1, 0, 4, 5, 6, 7) {}
    void setup();
    void display(String text);
};

#endif
