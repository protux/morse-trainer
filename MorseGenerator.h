#ifndef _MorseGenerator_h
#define _MorseGenerator_h

#define DIT '.'
#define DAH '-'
#define SYMBOL_SEPARATOR '#'
#define WORD_SEPARATOR '/'
#define MORSE_SYMBOL_SEPARATOR ' '

#define DIT_DURATION 60
#define DAH_DURATION DIT_DURATION * 3
#define WORD_SPACE_DURATION DIT_DURATION * 7
#define SYMBOL_SPACE_DURATION DIT_DURATION * 3
#define MORSE_SYMBOL_SPACE_DURATION DIT_DURATION

#include "MorseAlphabet.h"
#include "string.h"

class MorseGenerator {
    bool active = false;
    long timeSinceLastUpdate = 0;
    long timeOfLastUpdate = 0;
    byte morsePin;
    byte currentMessageIndex = -1;
    MorseAlphabet* morseAlphabet;
    String message;
    String currentSymbol;

    int getCurrentSymbolLength();
    void updateTimeSinceLastUpdate();
    void increaseMessageIndex();
    bool isSwitchSymbols();
    void loadNewMorseSymbols();
    void switchSymbols();
    int getDurationOfCurrentSymbol();
    int getVoltageStateOfCurrentSymbol();
    void stretchCurrentSymbol();

  public:
    MorseGenerator(MorseAlphabet* morseAlphabet, byte morsePin);
    void setup();
    void activate();
    void deactivate();
    bool isActive();
    void update();
    void reinitialize(String message);
};

#endif
