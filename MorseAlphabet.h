#ifndef _MorseAlphabet_h
#define _MorseAlphabet_h

#define SYMBOL_COUNT 40

#include "Arduino.h"
#include "string.h"

class MorseAlphabet {
    char* morseAlphabet[SYMBOL_COUNT][2] = {
      {'A', ".-"},
      {'B', "-..."},
      {'C', "-.-."},
      {'D', "-.."},
      {'E', "."},
      {'F', "..-."},
      {'G', "--."},
      {'H', "...."},
      {'I', ".."},
      {'J', ".---"},
      {'K', "-.-"},
      {'L', ".-.."},
      {'M', "--"},
      {'N', "-."},
      {'O', "---"},
      {'P', ".--."},
      {'Q', "--.-"},
      {'R', ".-."},
      {'S', "..."},
      {'T', "-"},
      {'U', "..-"},
      {'V', "...-"},
      {'W', ".--"},
      {'X', "-..-"},
      {'Y', "-.--"},
      {'Z', "--.."},
      {'1', ".----"},
      {'2', "..---"},
      {'3', "...--"},
      {'4', "....-"},
      {'5', "....."},
      {'6', "-...."},
      {'7', "--..."},
      {'8', "---.."},
      {'9', "----."},
      {'0', "-----"},
      {'.', ".-.-.-"},
      {',', "--..--"},
      {'?', "..--.."},
      {'=', "-...-"},
    };

    int findIndexOfSymbolInArray(char symbol);

  public:
    char* getMorseForSymbol(char symbol);
    char getRandomSymbol();
};

#endif
