#define BUZZER_PIN 5

#include "T9.h"
#include "Screen.h"
#include "MorseAlphabet.h"
#include "MorseGenerator.h"
#include "MorseToTextGame.h"

Screen screen;
T9 t9(&screen);
MorseAlphabet morseAlphabet;
MorseGenerator morseGenerator(&morseAlphabet, BUZZER_PIN);
MorseToTextGame morseToTextGame(&morseAlphabet, &morseGenerator, &t9);

void setup() {
//  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);

  digitalWrite(LED_BUILTIN, HIGH);
  screen.setup();
  t9.setup();
  morseGenerator.setup();
  randomSeed(analogRead(0));
}

void loop() {
  t9.update();
  morseGenerator.update();

  if (morseToTextGame.isActive()) {
    morseToTextGame.update();
  } else {
    morseToTextGame.start();
  }
}
