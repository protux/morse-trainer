#include "Screen.h"

void Screen::printRows(String text) {
  printRow(0, text.substring(0, SCREEN_COLUMNS));
  printRow(1, text.substring(SCREEN_COLUMNS, SCREEN_SIZE));
}

void Screen::printRow(int rowIndex, String chunk) {
  lcd.setCursor(0, rowIndex);
  lcd.print(chunk);
}

String Screen::cropTextBuffer(String text) {
  if (text.length() > SCREEN_SIZE) {
    return text.substring(text.length() - SCREEN_SIZE, text.length());
  }
  return text;
}

void Screen::setup() {
  lcd.begin(SCREEN_COLUMNS, SCREEN_ROWS);
  lcd.setCursor(0, 0);
  lcd.setBacklightPin(3,POSITIVE);
  lcd.setBacklight(HIGH);
}

void Screen::display(String text) {
  String textToPrint = cropTextBuffer(text);
  printRows(textToPrint);
}
