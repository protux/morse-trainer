#ifndef _MorseToTextGame_h
#define _MorseToTextGame_h

#include "Arduino.h"

#include "MorseAlphabet.h"
#include "MorseGenerator.h"
#include "T9.h"

class MorseToTextGame {
  String currentText;
  MorseAlphabet* morseAlphabet;
  MorseGenerator* morseGenerator;
  T9* t9;
  bool active;

  void generateNewText();
  void wrongAnswer();
  void correctAnswer();
  
  public:
    MorseToTextGame(MorseAlphabet* morseAlphabet, MorseGenerator* morseGenerator, T9* t9);
    void start();
    void stop();
    bool isActive();
    void answer(String answer);
    void update();
};

#endif
